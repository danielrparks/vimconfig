-- NVIM OPTIONS
vim.opt.undofile = true

vim.g.netrw_browsex_viewer = "gio open"
vim.g.mapleader = " "

-- Display
vim.opt.title = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.ruler = true

vim.opt.scrolloff = 3

-- Search
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.backspace = { "indent", "eol", "start" }

-- Syntax highlighting
vim.cmd([[
filetype on
filetype plugin on
filetype indent on
]])

vim.opt.hidden = true

-- Tabs
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = false
vim.opt.listchars = { eol = "$", tab = "——→", trail = "~", space = "·", nbsp = "|" }

vim.opt.splitright = true

-- misc mappings
vim.keymap.set("n", "Y", "y$")
vim.keymap.set("n", "<leader>v", "'[v']")
vim.keymap.set("n", "<leader>V", "'[V']")

vim.opt.mouse = "nvi"
-- idk why this one doesn't work in lua
vim.cmd("set mouseshape=\"n:beam,v:beam,o:beam,i:beam,r:beam,e:arrow,s:udsizing,sd:udsizing,vs:lrsizing,vd:lrsizing\"")
vim.opt.guifont = "Fira Code:h10"

vim.opt.termguicolors = true

vim.keymap.set("", "<ScrollWheelUp>", "<C-Y>")
vim.keymap.set("", "<S-ScrollWheelUp>", "<C-U>")
vim.keymap.set("", "<ScrollWheelDown>", "<C-E>")
vim.keymap.set("", "<S-ScrollWheelDown>", "<C-D>")
vim.keymap.set("", "<ScrollWheelRight>", "<zl>")
vim.keymap.set("", "<ScrollWheelLeft>", "<zh>")

vim.opt.backup = false
vim.opt.undodir = vim.fn.stdpath("config") .. "/tmp"

vim.api.nvim_create_user_command("Bd", "bp\\|bd \\#", {})
vim.api.nvim_create_user_command("BD", "bp\\|bd \\#", {})
vim.keymap.set("t", "<S-CR>", "<C-\\><C-n>")

function WC()
	local filename = vim.fn.expand("%")
	local cmd = "detex " .. filename .. " | wc -w | tr -d [:space:]"
	local result = vim.fn.system(cmd)
	print(result .. " words")
end
vim.api.nvim_create_user_command("WC", WC, {})

vim.g.tex_flavor = 'latex'
-- "autocmd Filetype tex set conceallevel=1
-- "let g:vimtex_syntax_conceal = {
-- "			\ 'accents': 1,
-- "			\ 'cites': 1,
-- "			\ 'fancy': 1,
-- "			\ 'greek': 1,
-- "			\ 'math_bounds': 1,
-- "			\ 'math_delimiters': 1,
-- "			\ 'math_fracs': 0,
-- "			\ 'math_super_sub': 0,
-- "			\ 'math_symbols': 1,
-- "			\ 'sections': 1,
-- "			\ 'styles': 1,
-- "			\}
vim.g.vimtex_fold_manual = 1
vim.g.vimtex_matchparen_enabled = 0
vim.g.vimtex_complete_enabled = 0
vim.g.vimtex_compiler_enabled = 0
vim.g.vimtex_view_enabled = 0
vim.cmd([[
autocmd Filetype tex setlocal spell
autocmd Filetype markdown setlocal spell
]])
vim.keymap.set("i", "<SNR>16_AutoPairsReturn", "=AutoPairsReturn()", { silent = true })
--
--
--
-- PLUGIN OPTIONS
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	"dracula/vim",
	{ "bluz71/vim-nightfly-colors", name = "nightfly", lazy = false, priority = 1000 },
	"neovim/nvim-lspconfig",
	"hrsh7th/nvim-cmp",
	"hrsh7th/cmp-buffer",
	"hrsh7th/cmp-nvim-lsp",
	"saadparwaiz1/cmp_luasnip",
	"L3MON4D3/LuaSnip",
	"tpope/vim-fugitive",
	{"jiangmiao/auto-pairs", lazy = false},
	"tpope/vim-endwise",
	"sirtaj/vim-openscad",
	"alvan/vim-closetag",
	"tpope/vim-characterize",
	{"nvim-treesitter/nvim-treesitter", build = ":TSUpdate"},
	"nvim-treesitter/playground",
	"editorconfig/editorconfig-vim",
	"nvim-lua/plenary.nvim",
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-telescope/telescope-fzy-native.nvim",
			"debugloop/telescope-undo.nvim",
		}
	},
	"ahmedkhalf/project.nvim",
	"tpope/vim-surround",
	"kyazdani42/nvim-web-devicons", -- for file icons
	"kyazdani42/nvim-tree.lua",
	"kmonad/kmonad-vim",
	"RRethy/nvim-align",
	"stevearc/dressing.nvim",
	"nfnty/vim-nftables",
	"jeetsukumaran/vim-indentwise",
	"mhartington/formatter.nvim",
	"moevis/base64.nvim",
	"mickael-menu/zk-nvim",
	"lewis6991/gitsigns.nvim",
	{ "ggandor/leap.nvim", lazy = false },
}, {lazy = true})

local nvim_lsp = require('lspconfig')
local opts = {noremap = true, silent = true}

-- Use an on_attach function to only map the following keys 
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
	local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
	local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

	--Enable completion triggered by <c-x><c-o>
	buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- Mappings.
	local opts = { noremap=true, silent=true }

	-- See `:help vim.lsp.*` for documentation on any of the below functions
	buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
	buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
	buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
	buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
	buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
	buf_set_keymap('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
	buf_set_keymap('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
	buf_set_keymap('n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
	buf_set_keymap('n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
	buf_set_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	buf_set_keymap('n', '<leader>ca', "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
	buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
	-- buf_set_keymap('n', 'gu', '<cmd>lua vim.lsp.buf.references()<CR>', opts) -- I like this better for lowercase
	buf_set_keymap('n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
	buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
	buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
	buf_set_keymap('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
	buf_set_keymap("n", "<leader>f", "<cmd>lua vim.lsp.buf.format()<CR>", opts)
	buf_set_keymap('n', 'gs', "<cmd>lua require'telescope.builtin'.lsp_workspace_symbols{}<CR>", opts)
end

vim.cmd("color dracula")

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { "pyright", "ts_ls", "rust_analyzer", "ansiblels", "bashls", "gopls" }
for _, lsp in ipairs(servers) do
	nvim_lsp[lsp].setup { 
		on_attach = on_attach;
	};
end

nvim_lsp.html.setup {
	on_attach = on_attach;
	capabilities = capabilities;
};
nvim_lsp.cssls.setup {
	on_attach = on_attach;
	capabilities = capabilities;
};
nvim_lsp.clangd.setup {
	on_attach = on_attach;
	cmd = { 'clangd', '--background-index' }
}
nvim_lsp.texlab.setup {
	-- cmd = {"texlab", "-vvvvv","--log-file", "/tmp/texlab.log"},
	on_attach = on_attach;
	settings = {
		texlab = {
			auxDirectory = "./.latexmk",
			build = {
				args = {"-lualatex" ,"-outdir=./.latexmk"},
				onSave = true,
				forwardSearchAfter = true
			},
			forwardSearch = {
				executable = "evince-synctex",
				args = {"-f", "%l", "%p", "\"code -g %f:%l\""}
			}
		}
	}
}

vim.o.completeopt = 'menuone,noselect'

local cmp = require 'cmp'
local types = require 'cmp.types'
local luasnip = require 'luasnip'

luasnip.setup({
	region_check_events = {"InsertEnter"}
})

cmp.setup {
	snippet = {
		expand = function(args)
			require('luasnip').lsp_expand(args.body)
		end,
	},
	mapping = {
		['<C-p>'] = cmp.mapping.select_prev_item(),
		['<C-n>'] = cmp.mapping.select_next_item(),
		['<C-d>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping.close(),
		['<CR>'] = function(fallback)
			entry = cmp.get_selected_entry()
			if luasnip.expand_or_locally_jumpable() then
				luasnip.expand_or_jump()
			else
				fallback()
			end
		end,
		['<Tab>'] = function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_locally_jumpable() then
				luasnip.expand_or_jump()
			else
				fallback()
			end
		end,
		['<S-Tab>'] = function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end,
	},
	sources = {
		{ name = 'nvim_lsp' },
		{ name = 'luasnip' },
		{ name = 'buffer' },
	},
	preselect = cmp.PreselectMode.None
}

require'my_snippets'

local t = function(str)
	return vim.api.nvim_replace_termcodes(str, true, true, true)
end

require'nvim-treesitter.configs'.setup {
	ensure_installed = {"bash", "bibtex", "c", "clojure", "cmake", "cpp", "css", "devicetree", "diff", "dockerfile", "go", "html", "java", "javascript", "json", "latex", "lua", "make", "python", "toml", "typescript", "vim", "xml", "yaml"},
	highlight = {
		enable = true,
	},
	indent = {
		enable = true,
	},
}

local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.openscad = {
	install_info = {
		url = "~/code/git/tree-sitter-openscad",
		files = {"src/parser.c"},
	}
}

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
	vim.lsp.diagnostic.on_publish_diagnostics, {
		severity_sort = true,
	}
)

require("project_nvim").setup {}

local telescope = require('telescope')
telescope.setup{
	defaults = {
		-- Default configuration for telescope goes here:
		-- config_key = value,
		-- ..
	},
	pickers = {
		-- Default configuration for builtin pickers goes here:
		-- picker_name = {
		--	 picker_config_key = value,
		--	 ...
		-- }
		-- Now the picker_config_key will be applied every time you call this
		-- builtin picker
	},
	extensions = {
		-- Your extension configuration goes here:
		-- extension_name = {
		--	 extension_config_key = value,
		-- }
		-- please take a look at the readme of the extension you want to configure
		undo = { }
	}
}
telescope.load_extension('fzy_native')
telescope.load_extension('projects')

require'my_nvimtree'

require'dressing'.setup{
	input = {
		insert_only = false,
	},
}

local prettier = require("formatter.defaults.prettier")
local futil = require("formatter.util")
require("formatter").setup{
	logging = true,
	log_level = vim.log.levels.WARN,
	filetype = {
		html = { prettier, },
		javascript = { prettier, },
		typescript = { futil.withl(prettier, "typescript"), },
		json = { futil.withl(prettier, "json"), },
		css = { futil.withl(prettier, "css"), },
		xml = {
			function()
				return {
					exe = "xmllint",
					args = {
						"--format",
						futil.escape_path(futil.get_current_buffer_file_path()),
					},
					stdin = true,
				}
			end
			},
	}
}

require("gitsigns").setup{
	signcolumn = false,
}

require("zk").setup({
	picker = "telescope",
})
vim.api.nvim_set_keymap("n", "<leader>zn", "<Cmd>ZkNew { title = vim.fn.input('Title: ') }<CR>", opts) -- Create a new note after asking for its title.

vim.api.nvim_set_keymap("n", "<leader>zo", "<Cmd>ZkNotes { sort = { 'modified' } }<CR>", opts) -- Open notes.

vim.api.nvim_set_keymap("n", "<leader>zt", "<Cmd>ZkTags<CR>", opts) -- Open notes associated with the selected tags.

vim.api.nvim_set_keymap("n", "<leader>zf", "<Cmd>ZkNotes { sort = { 'modified' }, match = { vim.fn.input('Search: ') } }<CR>", opts) -- Search for the notes matching a given query.
vim.api.nvim_set_keymap("v", "<leader>zf", ":'<,'>ZkMatch<CR>", opts) -- Search for the notes matching the current visual selection.

local telescope_treeroot = function(f, opts)
	opts = opts or {}
	local api = require('nvim-tree.api')
	if api.tree.is_visible() then
		cwd = api.tree.get_nodes().absolute_path
		opts["cwd"] = cwd
	end
	f(opts)
end

local telescope_subdir = function(f, opts)
	opts[cwd] = vim.fn.expand("%:p:h")
	f(opts)
end

local is_any_visual_mode = function(m)
	return m == 'v' or m == 'V' or m == ''
end

local wrap_opts_default_text = function(opts)
	if is_any_visual_mode(vim.fn.mode()) then
		local text = table.concat(vim.fn.getregion(vim.fn.getpos('v'), vim.fn.getpos('.'), { type = vim.fn.mode() }), "\n")
		opts["default_text"] = text
	end
	return opts
end

local tele = function(t)
	local target = t.func or require'telescope.builtin'[t.t]
	local opts = t.opts or {}
	if t.wrap then
		return function() t.wrap(target, wrap_opts_default_text(opts)) end
	else
		return function() target(wrap_opts_default_text(opts)) end
	end
end

vim.keymap.set("n", "<leader>Gf", tele{ t = "find_files" }, opts)
vim.keymap.set("n", "<leader>Gr", tele{ t = "live_grep" }, opts)
vim.keymap.set("n", "<leader>gf", tele{ t = "find_files", wrap = telescope_treeroot }, opts)
vim.keymap.set("n", "<leader>gr", tele{ t = "live_grep", wrap = telescope_treeroot }, opts)
vim.keymap.set("n", "<leader>sgr", tele{ t = "live_grep", wrap = telescope_subdir }, opts)
vim.keymap.set("n", "<leader>gm", tele{ t = "man_pages" }, opts)
vim.keymap.set("n", "<leader>gc", tele{ t = "tags" }, opts)
vim.keymap.set("n", "<leader>gt", tele{ t = "treesitter" }, opts)

vim.keymap.set("v", "Gf", tele{ t = "find_files" }, opts)
vim.keymap.set("v", "Gr", tele{ t = "live_grep" }, opts)
vim.keymap.set("v", "gf", tele{ t = "find_files", wrap = telescope_treeroot }, opts)
vim.keymap.set("v", "gr", tele{ t = "live_grep", wrap = telescope_treeroot }, opts)
vim.keymap.set("v", "sgr", tele{ t = "live_grep", wrap = telescope_subdir }, opts)
vim.keymap.set("v", "gm", tele{ t = "man_pages" }, opts)
vim.keymap.set("v", "gc", tele{ t = "tags" }, opts)
vim.keymap.set("v", "gt", tele{ t = "treesitter" }, opts)

vim.keymap.set("n", "<C-n>", "<cmd>NvimTreeFocus<CR>", opts)
vim.keymap.set("n", "<leader>n", "<cmd>NvimTreeFindFile<CR>", opts)
vim.keymap.set("n", "<leader>u", "<cmd>Telescope undo<cr>")

vim.keymap.set({'n', 'x', 'o'}, 'gs',  '<Plug>(leap-forward)')
vim.keymap.set({'n', 'x', 'o'}, 'gS',  '<Plug>(leap-backward)')
